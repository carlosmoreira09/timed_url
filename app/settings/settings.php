<?php

/**
 * All settings here will be available via the SlimContainer
 */

return [
    'settings' => [
        'appUrl' => getenv('APP_URL'),
        'displayErrorDetails' => getenv('DISPLAY_ERRORS'),
        'db' => [
            'driver' => getenv('DB_DRIVER'),
            'host' => getenv('DB_HOST'),
            'database' => getenv('DB_NAME'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => getenv('DB_PREFIX'),
        ]
    ],
];