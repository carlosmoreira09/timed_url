<?php

/**
 * Global Services
 */

/**
 * @var $container \Interop\Container\ContainerInterface
 * @var $capsule Illuminate\Database\Capsule\
 */

/**
 * @param $container
 * @return \Illuminate\Database\Capsule\
 */
$container['db'] = function ($container) use ($capsule){
    return $capsule;
};

/**
 * @param $container
 * @return \Slim\Views\PhpRenderer
 */
$container['view'] = function ($container){
    return new \Slim\Views\PhpRenderer('../app/http/views/');
};

$container['randomslug'] = function ($container){
    return uniqid(\app\models\Redirector::all()->count());
};