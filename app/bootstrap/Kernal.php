<?php
/**
 * Created by PhpStorm.
 * User: CarlosMoreira
 * Date: 8/22/2018
 * Time: 9:13 PM
 */

/**
 * @todo:
 * Modify the kernal to allow for a seperate file for services.
 *
 * include services
 * $container = $app->getContainer();
 * ...add to container $container['myService'] = function(){}
 *
 * Figure out how to load the settings into the container before hand
 *
 * Maybe when bootstrapping... have a settings .php page
 *
 */

namespace app\bootstrap;

use Slim\App;

class Kernal
{
    const SERVICES = [

    ];

    public function __construct(){}

    public static function instantiate_db($container){
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection($container['settings']['db']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        return $capsule;
    }

    private function getSettings(){
        return require ( __DIR__ . '/../settings/settings.php') ;
    }

    /**
     * @return App
     */
    public function runApp(){
        $app = new App($this->getSettings());
        require_once __DIR__ . '/../http/routes.php';
        $container = $app->getContainer();
        $capsule = self::instantiate_db($container);
        require ( __DIR__ . '/../services/services.php') ;
        $app->run();
    }

}