<?php
/**
 * Created by PhpStorm.
 * User: CarlosMoreira
 * Date: 8/22/2018
 * Time: 9:19 PM
 */

/**
 * @var $app Slim\App
 */

$app->get('/', function(\Slim\Http\Request $request, \Psr\Http\Message\ResponseInterface $response){
    return $this->view->render($response, "home.php");
});

$app->post(
    '/redirector/store',
    \app\http\controllers\RedirectorController::class . ':store'
);

$app->get('/tmt/{slug}',
        \app\http\controllers\RedirectorController::class . ':redirect'
    )->setName('tmt');