<?php
/**
 * Created by PhpStorm.
 * User: CarlosMoreira
 * Date: 8/23/2018
 * Time: 8:23 PM
 */

namespace app\http\controllers;

use app\models\Redirector;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Carbon\Carbon;

class RedirectorController extends BaseController
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Create a new redirector url
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function store(Request $request, Response $response){
        try{
            $this->validate($request, ['url', 'time_length', 'time_notation']);
            $time_length = (int) $request->getParsedBodyParam('time_length');
            $time_notation = $request->getParsedBodyParam('time_notation');
            if($time_notation != 'hours' && $time_notation != 'minutes'){
                throw new \Exception('time_notation must be hours or minutes');
            }

            $dateTime = new \DateTime();
            $dateTime->modify("+{$time_length} {$time_notation}");

            $redirector = new Redirector();
            $redirector->url = $request->getParsedBodyParam('url');
            $redirector->slug = $this->container->randomslug;
            $redirector->expire_at = $dateTime->format('Y-m-d H:i:s');
            $redirector->save();

            $response = $response->withJson([
                'success' => 'ok',
                'url' => $this->container->settings['appUrl'] . '/tmt/' . $redirector->slug
            ]);
            sleep(2);
            return $response;
        }catch(\Exception $exception){
            return $response = $response->withJson([
                'error' => $exception->getMessage()
            ]);
        }
        
    }

    /**
     * Redirect the user to appropriate site
     * @param Request $request
     * @param Response $response
     */
    public function redirect(Request $request, Response $response, $args){
        try{
            $redirector = Redirector::where('slug',$args['slug'])->first();
            
            $expireAt = Carbon::parse($redirector->expire_at);
            if($expireAt->isPast()){
                return $this->container->view->render($response, "404.php");
            }
                
            return $response->withRedirect($redirector->url);
        }catch(\Exception $exception){
            return $this->container->view->render($response, "404.php");
        }
        
    }
}