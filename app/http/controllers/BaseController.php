<?php
/**
 * Created by PhpStorm.
 * User: CarlosMoreira
 * Date: 8/23/2018
 * Time: 8:23 PM
 */

namespace app\http\controllers;

use Slim\Http\Request;

class BaseController{

    const HEADER_JSON = "application/json";

    public function __construct()
    {
    }

    protected function validate(Request $request, $validators){
        if($request->getHeaderLine("Content-Type") == BaseController::HEADER_JSON){
            $requestData = $request->getParsedBody();
            foreach($validators as $validator){
                if(!isset($requestData[$validator])){
                    throw new \Exception("Missing Request " . $validator );
                }
            }
        }
    }
}