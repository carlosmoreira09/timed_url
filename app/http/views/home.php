<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Timed Url</title>
    <link rel="stylesheet" href="/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/libs/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script src="/libs/vue/vue.js"></script>
</head>
<body class="Site" >
    <main class="Site-content d-flex">
        <div v-cloak id="app" class="container d-flex  justify-content-center align-self-center">

    <div>

        <div class="row d-flex justify-content-center align-items-center flex-column ">
            <div class="col-md-12 text-center text-white" id="logo">
                <h2>Timed URL</h2>
                <h5>Create | Limit | Share</h5>
            </div>
            <br>
        </div>

        <div class="row d-flex justify-content-center align-items-center flex-column ">
            <form action="" style="width:50%">
                <div class="input-group">
                    <input v-model="timed_url.url" type="text" class="form-control" placeholder="Your URL" aria-label="Your URL" aria-describedby="basic-addon2" :disabled="loading">

                    <div class="input-group-append">
                        <button @click="create()" class="btn btn-success" type="button" :disabled="loading || !isValidUrl" >
                            Do it! <i v-if="loading" class="fa fa-spinner fa-spin"></i>
                        </button>
                    </div>
                </div>
                <br>

                <div class="input-group d-flex justify-content-center">
                        <div class="col-md-3 d-flex align-items-center">
                            <label for="time_length" class="text-white">How Long?</label>
                        </div>
                        <div class="col-md-3">
                            <input v-mask="'###'" v-model="timed_url.time_length" class="form-control" type="text" id="time_length" placeholder="5" :disabled="loading">
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" v-model="timed_url.time_notation" :disabled="loading">
                                <option value="minutes">Minutes</option>
                                <option value="hours">Hours</option>
                            </select>
                        </div>

                </div>
            </form>
        </div>

        <br>

        <div class="row d-flex justify-content-center align-items-center flex-column ">
            <div class="alert alert-warning alert-dismissible fade show" role="alert" v-if="shortenedUrl">
                <strong>Your URL:</strong> <a id="shortend_url" target="_blank">{{shortenedUrl}}</a>
                <button id="copyToClipboard" title="Copy to Clipboard" type="button" class="close" aria-label="Close" :data-clipboard-text="shortenedUrl">
                    <span aria-hidden="true"><i class="fa fa-copy"></i></span>
                </button>
            </div>
            <div class="alert alert-danger alert-dismissible fade show" v-if="error">
                <i class="fa fa-exclamation-circle"></i> {{error}}
            </div>
        </div>

        <hr class="sweet-line">

        <div class="row d-flex justify-content-center align-items-center text-white fjalla-one-font">
            <div class="col-3">
                <span class="step">#1</span>
            </div>
            <div class="col-3">
                <span class="step">#2</span>
            </div>
            <div class="col-3">
                <span class="step">#3</span>
            </div>
        </div>
        <div class="row text-center d-flex justify-content-center align-items-center text-white fjalla-one-font bottom-boxes">
            <div class="col-3">
                <p>Add URL</p>
            </div>
            <div class="col-3">
                <p>Select Time Limit</p>
            </div>
            <div class="col-3">
                <p>Share Until Expire</p>
            </div>
        </div>
    </div>
</div>
    </main>

<footer class="footer text-white text-center">
    <p>Copyright @ Carlos 2018</p>
</footer>
<script src="/libs/axios/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/v-mask/1.3.2/v-mask.min.js"></script>
</script>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>