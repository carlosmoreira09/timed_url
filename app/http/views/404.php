<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport"
		content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Timed Url: 401</title>
		<link rel="stylesheet" href="/libs/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/libs/font-awesome-4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<style type="text/css">
			body,html {
				height: 100%;
			}
			body {
				background-attachment:scroll;
				background-repeat:no-repeat;
				background-position:center;
				background-size:cover;
				line-height:5px;
			}
			.display-1 {text-align:center;color:#e1b7b7;}
			.display-1 .fa {animation:fa-spin 5s infinite linear;}
			.display-3 {text-align:center;color:#df726a;}
			.lower-case {text-align:center;}
		</style>
	</head>
	<body>
		<div class="container d-flex h-100">
			<div class="row align-self-center w-100">
				<div class="col-6 mx-auto">
					<div class="jumbotron">
						<h1 class="display-1">4<i class="fa  fa-spin fa-cog fa-3x"></i> 1</h1>
						<h1 class="display-3">ERROR</h1>
						<p class="lower-case">Aw !! Webpage not found :(</p>
						<p class="lower-case">Maybe the page has blown up.</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>