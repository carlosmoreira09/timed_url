<?php
/**
 * Created by PhpStorm.
 * User: CarlosMoreira
 * Date: 8/22/2018
 * Time: 8:23 PM
 */

require '../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv( '../');
$dotenv->load();

if(env('environment') !== 'PROD')
    ini_set('display_errors', '1');

$kernal = new \app\bootstrap\Kernal();
$kernal->runApp();