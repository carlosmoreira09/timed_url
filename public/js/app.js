Vue.directive('mask', VueMask.VueMaskDirective);

new Vue({
    el: '#app',
    created(){
        var clipboard = new ClipboardJS('#copyToClipboard');

        clipboard.on('success', function(e) {
            e.clearSelection();
        });

        clipboard.on('error', function(e) { 
        	//Do something on error
        });
    },
    data: {
        'shortenedUrl' : '',
        'timed_url': {
            'url' : '',
            'time_length' : 5,
            'time_notation' : 'minutes'
        },
        'loading' : false,
        'error' : null
    },
    computed: {
        isValidUrl : function ValidURL() {
            var a  = document.createElement('a');
            a.href = this.timed_url.url;
            return (a.host && a.host != window.location.host);
        }
    },
    methods: {
        create : function(){
            this.error = null;
            
            if(this.timed_url.time_length <= 0){
                this.error = "Time length needs to be greater than 0";
                return;
            }

            this.loading = true;
            axios.post('/redirector/store', this.timed_url).then((response) => {
                console.log(response);
                this.loading = false;
                if(response.data.error){
                    this.error = response.data.error;
                }
                if(response.data){
                    this.shortenedUrl = response.data.url;
                    //Reset the form
                    this.timed_url = {
			            'url' : '',
			            'time_length' : 5,
			            'time_notation' : 'minutes'
        			}
                }
            }).catch((response) => {
                console.log(response);
                this.loading = false;
            });
        },
        
    }
})